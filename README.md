# Invoices manager

The goal of this project is to manipulate large JSON files acquired from API calls and organize them in a certain format.

The project is in python 3.6 & uses Pandas and Jupyter notebook.