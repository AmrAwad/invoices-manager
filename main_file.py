

'''
The overall objective is to create invoices for affiliates this is done by 1) get an array of affiliate_ids which have outstanding balances above a certain amount. This is calculated as sales commissions plus referral commissions minus all time payout. these numbers are found in three seperate json files.
Then filter this array with a few if statements. 


The second part is to take this array of affiliate_ids and create invoices for them. this is done in three parts. 1) find the start date as day after last invoice, 2) find the invoice stats from that start date until last sunday
3) create an empty invoice 4) add the invoice items from the invoice stats into it. 







#   Part 1, get array of affiliate_id to make invoices for. Those who have outstanding balaces above their minimum payment. 

# Step 1, Affiliates earn both sales_comission and referral_comission. Their earnings are from two api calls.
#  I would need to join/consolidate 1_getStats.json and 2_getAffiliateCommissions.json by "affiliate_id"= "referral_id", some affiliates exist in only the sales commission, some only in the referral, some both. 

# The first call, Result saved in getStats.json


sales_commission =  'https://company_11.api.hasoffers.com/Apiv3/json?NetworkToken=NETworkToken&'\
    'Target=Report&'\
    'Method=getStats&'\
    'limit=10000&'\
    'fields[]=Stat.revenue&'\
    'fields[]=Stat.affiliate_manager_id&'\
    'fields[]=Stat.payout&'\
    'fields[]=Stat.affiliate_id&'\
    'fields[]=Affiliate.company&'\
    'fields[]=AffiliateManager.full_name&'\
    'filters[Stat.affiliate_id][conditional]=EQUAL_TO&'\
    'filters[Stat.affiliate_id][values][0]=8966&'\
    'filters[Stat.affiliate_id][values][1]=8404&'\
    'filters[Stat.affiliate_id][values][2]=1594&'\
    'filters[Stat.affiliate_id][values][3]=2096&'\
    'filters[Stat.affiliate_id][values][4]=3018&'\
    'filters[Stat.payout][conditional]=GREATER_THAN&'\
    'filters[Stat.payout][values]=50&'\
    'filters[Stat.conversions][conditional]=GREATER_THAN&'\
    'filters[Stat.conversions][values]=1&'\
    'sort[Stat.payout]=desc&totals=1&'\
    'currency=USD&'\
    'data_start=2017-01-02&'\
    'data_end=2018-07-13'#\
#    + (dt.date.today() - dt.timedelta( (dt.date.today().weekday() + 1) % 7 )).strftime('%Y-%m-%d') this is last sunday


sales_commission =  
https://company_11.api.hasoffers.com/Apiv3/json?NetworkToken=NETworkToken&Target=Report&Method=getStats&limit=10000&fields[]=Stat.revenue&fields[]=Stat.affiliate_manager_id&fields[]=Stat.payout&fields[]=Stat.affiliate_id&fields[]=Affiliate.company&fields[]=AffiliateManager.full_name&filters[Stat.affiliate_id][conditional]=EQUAL_TO&filters[Stat.affiliate_id][values][0]=8966&filters[Stat.affiliate_id][values][1]=8404&filters[Stat.affiliate_id][values][2]=1594&filters[Stat.affiliate_id][values][3]=2096&filters[Stat.affiliate_id][values][4]=3018&filters[Stat.payout][conditional]=GREATER_THAN&filters[Stat.payout][values]=50&filters[Stat.conversions][conditional]=GREATER_THAN&filters[Stat.conversions][values]=1&sort[Stat.payout]=desc&totals=1&currency=USD&data_start=2017-01-02&data_end=2018-07-13



# The second call, results saved in getAffiliateCommissions.json


referral_call = 'https://company_11.api.hasoffers.com/Apiv3/json?NetworkToken=NETworkToken&'\
    'Target=Report&'\
    'Method=getAffiliateCommissions&'\
    'limit=10000&'\
    'fields[]=Stat.amount&'\
    'fields[]=Stat.referral_id&'\
    'fields[]=Affiliate.company&'\
    'filters[Stat.referral_id][conditional]=EQUAL_TO&'\
    'filters[Stat.referral_id][values][0]=8966&'\
    'filters[Stat.referral_id][values][1]=8404&'\
    'filters[Stat.referral_id][values][2]=1594&'\
    'filters[Stat.referral_id][values][3]=2096&'\
    'filters[Stat.referral_id][values][4]=3018&'\
    'data_start=2016-01-02&'\
    'data_end=2018-07-13'#\
#    + (dt.date.today() - dt.timedelta( (dt.date.today().weekday() + 1) % 7 )).strftime('%Y-%m-%d')


referral_call = 
https://company_11.api.hasoffers.com/Apiv3/json?NetworkToken=NETworkToken&Target=Report&Method=getAffiliateCommissions&limit=10000&fields[]=Stat.amount&fields[]=Stat.referral_id&fields[]=Affiliate.company&filters[Stat.referral_id][conditional]=EQUAL_TO&filters[Stat.referral_id][values][0]=8966&filters[Stat.referral_id][values][1]=8404&filters[Stat.referral_id][values][2]=1594&filters[Stat.referral_id][values][3]=2096&filters[Stat.referral_id][values][4]=3018&data_start=2016-01-02&data_end=2018-07-13




# Step 2, Change getStats.Stat.payout to sales_commission  and getAffiliateCommissions.Stat.amount to referral_commission

# Step 3, Collect all stats into one group per affiliate, So there is: affiliate_id, affiliate_company, sales_commission, referral_commission, affiliate_manager, affiliate_manager_id 

# Step 4, Make a new field "total_earnings" as sales_commission + referral_commission grouped by affiliate_id, i.e. for each affiliate add the payout from the sales table to the amount from the commission table.

# Step 5 add in some conditions that will remove affiliates
#   If affiliate_manager_id IN (24,26)     then drop all observations with earnings less than $50
#   If affiliate_manager_id NOT IN (27,28) then drop all observations with earnings less than $200



# Step 6, some affiliates get paid monthly.  add in the end of the month condition such that only calculates for ids on the first monday following the end of a month.
# the ids with this condition is when payment_terms = NET 30 and can be found in file findAllIds_complete.json


# Step 7, remove all manual invoicing accounts. same format as above. saved in file findAllIds_complete.json 
this condition is when "payment_terms": "invoice" (the accounts we are using I think are all manual, maybe just add this condition in and dont use it, or change the data for those ids)



# Step 8, create an array (or whatever you think is best) of all the affiliate_ids that remain/meet the above conditions and call it .. total_earnings_array = [2222, 3333, 6666, ....]






# Step 9

#  To get all time payments made, Create a loop to call API one affiliate_id at a time from the total_earnings_array, and collect responses in a dict/dataframe or whatever is best with affiliate_id, start_date, end_date, total_payout,timezone, error_messages, httpStatus call this dict/dataframe total_payout

# The API limit is 4 calls per second, please add in time.sleep(0.26) to the loop

# The point is that the API allows only one affiliate _id to be called per call, Im guessing the loop should look something like the below loop

# the results of a single call is saved in getPayoutsTotal.json

'''

for affiliate_id in total_earnings_array:
    payout_call = 'https://company_11.api.hasoffers.com/Apiv3/json?NetworkToken=NETwork_id_here&'\
        'Target=Billing&'\
        'Method=Payouts&'\
        'affiliate_id='+str({affiliate_id})+'&'\
        'timeframes[0][start_date]=2016-01-01&'\
        'timeframes[0][label]=week&'\
        'timeframes[0][end_date]='+(dt.date.today() - dt.timedelta( (dt.date.today().weekday() + 1) % 7 )).strftime('%Y-%m-%d')+'bn

    response = requests.get(payout_call)

    payout = json.loads(response.text)
    payout["affiliate_id"] = payout["request"]["affiliate_id"]
    payout["start_date"]   = payout["response"]["data"]["week"]["start_date"]
    payout["end_date"]     = payout["response"]["data"]["week"]["end_date"]
    payout["total_payout"] = payout["response"]["data"]["week"]["end_date"]


'''



# Now, we have to calculate account balances, i.e. total_earnings minus total_payouts for each affiliate_id

# From all the id's in the total_earnings_array or the table/json made in step 1 join in total_payout so we have one table/json with:
#   affiliate_id, affiliate_company, sales_commission, referral_commission, total_earnings, total_payout, start_date, end_date, affiliate_manager, affiliate_manager_id
#   Create another field called account_balance = total_earnings - total_payout.  



# Step 8 (Similar to step 5 above) add in some conditions that will remove/filter out affiliates but this time with account_balance
#   If affiliate_manager_id IN (24,26)     then drop all observations with account_balance less than $50
#   If affiliate_manager_id NOT IN (24,26) then drop all observations with account_balance less than $200
#   If affiliate_id IN (2222, 3333, ....) then drop if account_balance < $100 , If affiliate_id IN (4444, 5555, ....) then drop if account_balance < $200 ,

# create an array (or whatever you think is best) of all the affiliate_ids that remain/meet the above conditions and call it .. account_balance_array = [2222, 3333, 6666, ....]
 


# Creating Invoices with the account_balance_array. four step process detailed below,

this could be done one affiliate_id at a time through all four calls. or, all of the affiliate_ids through each call with storing info about the previous. 



Accounts to work with,  
    1) 8966 '8966'    Internal media buy, very simple only sales
    2) 8404 'affiliate_ref_and_sales'    both referral and comission,
    3) 1594 'yiyitest'   crazy amount of invoice items
    4) 2096  GG1

The API limit is 4 calls per second, please add in time.sleep(0.26) after each call. 




1) findInvoiceStats, if sum of "amount" on each level is  <= amount for manager ( alike above if manager id in (26,28,...)  then $50 minimum. if not in (26,28,...)  then $200 minimum ) then terminate and add line to .txt report. 

from this we need: 

the end date is last sunday (dt.date.today() - dt.timedelta( (dt.date.today().weekday() + 1) % 7 )).strftime('%Y-%m-%d')
and affiliate id



there are two examples findInvoiceStats_8404.json and findInvoiceStats_with_referral.json

find_invoice_stats = 'https://company_11.api.hasoffers.com/Apiv3/json?NetworkToken=NETworkToken&'\
    'Target=AffiliateBilling&Method=findInvoiceStats&'\
    'affiliate_id='+str({affiliate_id})+'&'\
    'end_date='+(dt.date.today() - dt.timedelta( (dt.date.today().weekday() + 1) % 7 )).strftime('%Y-%m-%d')+'&'\
    'currency=USD'


from here we take each level of "Stat" in "data"   find_invoice_stats["response"]["data"]["data"][0] and find_invoice_stats["response"]["data"]["data"][1]  for all levels, each is diffrent. 

in each find_invoice_stats["response"]["data"]["data"]["Stat"] we take out "type", "offer_id", "payout_type", "conversions" , "amount" 


2) Create empty invoice, to be filled with stats from above. 

start_date = start_date from find_invoice_stats["response"]["data"]["start_date"]   or if not exist findAllIds_complete date_added
end_date = last sunday
affiliate_id = all the ids which met specified criteria. 

create_invoice = 'https://company_11.api.hasoffers.com/Apiv3/json?NetworkToken=NETworkToken&'\
    'Target=AffiliateBilling&'\
    'Method=createInvoice&'\
    'data[end_date]='+(dt.date.today() - dt.timedelta( (dt.date.today().weekday() + 1) % 7 )).strftime('%Y-%m-%d')+'&'\
    'data[start_date]= start date from above &'\  
    'data[affiliate_id]='+str({affiliate_id})+'&'\
    'allow_duplicate=0'


Response for the above in createInvoice.json all you need is the invoice id from this part for the next part. 


3) addInvoiceItem   , need to take all the levels of find_invoice_stats["response"]["data"]["data"]["Stat"] and create invoice items with them

the invoice id is = createInvoice["response"]["data"]["AffiliateInvoice"]["id"]

Here an action is the payout type, i.e. if we have 30 conversions, then action = 30 , and set the payout type. 

The below example works It allows for duplicate entries, be careful!! 

create a loop, which will go over each level in the findInvoiceStats and create an invoice item for it. 

add_invoice_item = 'https://company_11.api.hasoffers.com/Apiv3/json?NetworkToken=NETworkToken'\
    '&Target=AffiliateBilling&Method=addInvoiceItem'\
    '&data[type]=stats'
    '&invoice_id=10613'\
    '&data[payout_type]=cpa_percentage'\
    '&data[offer_id]=668'\
    '&data[actions]=38'\
    '&data[amount]=950'\
    '&data[conversions]=null'
    


















DONE  AND ON TO THE NEXT ONE. 










'''













